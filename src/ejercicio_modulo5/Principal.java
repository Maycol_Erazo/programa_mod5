/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio_modulo5;

import helpers.Circulo;
import helpers.Cuadrado;
import helpers.Linea;
import helpers.Triangulo;


/**
 *
 * @author mike
 */
public class Principal {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
           
       Circulo frm= new Circulo();
       frm.imprimirInformacion();
       frm.establecerDiametro(10);
       frm.imprimirRadio();
       System.out.println("\n");
       
       Linea lin= new Linea();
       lin.imprimirInformacion();
       lin.establecerLargo(10.3); 
       lin.imprimirLargo();
       System.out.println("\n");
       
       Triangulo tri= new Triangulo();
       tri.imprimirInformacion();
       tri.establecerBase(8);
       tri.establecerAltura(10);
       tri.imprimirAreaTriangulo();
       System.out.println("\n");
       
       Cuadrado cua= new Cuadrado();
       cua.imprimirInformacion();
       cua.establecerLado(4); 
       cua.imprimirArea();
              
    }
    
}
