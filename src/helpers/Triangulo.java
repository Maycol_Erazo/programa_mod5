/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpers;

/**
 *
 * @author mike
 */
public class Triangulo extends Formas{

    private String angulos;
    private double area;
    private double base;
     private double altura;
    public Triangulo(){
          
   establecerDibujar("Triangulo"); 
   establecerColor("Azul");
   
    }
    
     public void establecerBase(double base_){
    this.base=base_; 
      
    }
    public double obtenerBase(){
    return this.base;
    
    }
     public void establecerAltura(double altura_){
    this.altura=altura_; 
      
    }
    public double obtenerAltura(){
    return this.altura;
    
    }
    public void imprimirAreaTriangulo(){
        area = (base*altura)/2;
        System.out.println("Área: "+ area);
      } 
}
